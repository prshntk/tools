2 + 3
x = sqrt(2);
x
s = "name"
pi
e
a = [ 1 2; 3 4; 5 6 ]
a'
a + 2
a * 3
#3 / a
g = [1 3 7]
#h = 3 / g
a .+ 2
b = [ 1 2 3; 4 5 6 ]
c = a * b
#a ^ 2
c ** 2
c .^ 2
d = [10 11; 12 13; 14 15 ]
e = d ./ a
inverse(c)
#inverse(a)
c(2,1)
c(:,1)
g = zeros(3,4)
for i = 1:3
	for j = 1:4
		g(i,j) = i + j;
	end
end
x = linspace(-pi, pi, 100);
y = sin(x);
plot(x, y, '+-', x, cos(x), 'o-');
title('Sin Cos');
legend('sine', 'cosine');
xlabel('x');
ylabel('f(x)');
data = load('data')
figure
plot(data(:,1), data(:,2), '+');
print('example_m.eps','-depsc');
