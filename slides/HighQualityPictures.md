% High quality pictures in LaTeX
% Piyush P Kurur
% March 30, 2015

# Drawing pictures and graphs

## Vector graphics

- Description of pictures via Mathematics

- Infinite scalability

- Metafont, metapost

## PGF/TikZ

- PGF is a vector graphics library in TeX

- TikZ is a library on top of PGF.

- Easy to integrate in your document

- Disadvantage: slow to build large pictures.


## TikZ libraries

- Shapes
- Automata
- trees

## Graphviz

- Give graph symbolically
- various layout algorithms: dot, neato, circo etc.
- formats: svg, tikz, ps etc.

## Use directly in LaTeX

- via the dot2texi package

- can use tikz for rendering.

- command 'pdflatex --shell-escape'
